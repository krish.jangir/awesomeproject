#!/bin/sh

#  appcenter-pre-build.sh
#  cicd
#
#  Created by krish on 05/01/22.
#  
#!/usr/bin/env bash

    BUILED_NUMBER_MAJOR=$((Major))
    BUILED_NUMBER_MINOR=$((Minor))
    BUILED_NUMBER_PATCH=$((Patch))
    APPCENTER_BUILD_ID = $((APPCENTER_BUILD_ID))
 
 #Version number by combination of appcenter variables
 plutil -replace CFBundleShortVersionString -string "\$(BUILED_NUMBER_MAJOR).\$(BUILED_NUMBER_MINOR).\$(BUILED_NUMBER_PATCH)" $APPCENTER_SOURCE_DIRECTORY/mysample/Info.plist
 #Build Number (Appcenter Build ID)
 plutil -replace CFBundleVersion -string "\$(APPCENTER_BUILD_ID)" $APPCENTER_SOURCE_DIRECTORY/mysample/Info.plist
